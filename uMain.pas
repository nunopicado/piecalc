unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, uPIECalc,
  FMX.DateTimeCtrls, FMX.StdCtrls, FMX.Edit, FMX.Controls.Presentation,
  FMX.Objects, FMX.Layouts, FMX.Effects;

type
  TfMain = class(TForm, IPIECalcView)
    rectHeader: TRectangle;
    lblTotalAmount: TLabel;
    lblExpirationDate: TLabel;
    edTotalDebt: TEdit;
    lblPaymentValue: TLabel;
    edPaymentValue: TEdit;
    lblNewBlockingDate: TLabel;
    bCalc: TButton;
    lblTitle: TLabel;
    pkExpirationDate: TDateEdit;
    pkNewBlockingDate: TDateEdit;
    lblRemainingDebt: TLabel;
    edRemainingDebt: TEdit;
    StyleBook1: TStyleBook;
    lblCurrentBlockingDate: TLabel;
    pkCurrentBlockingDate: TDateEdit;
    scrMain: TVertScrollBox;
    imgLogo: TImage;
    ShadowEffect1: TShadowEffect;
    GlowEffect1: TGlowEffect;
    procedure bCalcClick(Sender: TObject);
    procedure bCalcTap(Sender: TObject; const [Ref] Point: TPointF);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function ValueSanitacion(Value: String): Currency;
  public
    function Result(NewBlockingDate: TDateTime; RemainingDebt: Currency): IPIECalcView;
  end;

var
  fMain: TfMain;

implementation

{$R *.fmx}

uses Obj.SSI.IMaybe;

{ TfMain }

procedure TfMain.bCalcClick(Sender: TObject);
begin
     if edTotalDebt.Text.IsEmpty or edPaymentValue.Text.IsEmpty
        then Exit;
     TPIECalc.New(Self, pkExpirationDate.Date, pkCurrentBlockingDate.Date, ValueSanitacion(edTotalDebt.Text), edPaymentValue.Text.ToExtended).Eval;
end;

procedure TfMain.bCalcTap(Sender: TObject; const [Ref] Point: TPointF);
begin
     bCalcClick(Sender)
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
     pkCurrentBlockingDate.Date := Date;
     pkExpirationDate.Date      := Date;
     pkNewBlockingDate.Date     := Date;
end;

function TfMain.Result(NewBlockingDate: TDateTime; RemainingDebt: Currency): IPIECalcView;
begin
     pkNewBlockingDate.Date := NewBlockingDate;
     edRemainingDebt.Text   := FloatToStrF(RemainingDebt, ffCurrency, 8, 2);
end;

function TfMain.ValueSanitacion(Value: String): Currency;
begin
     Value := StringReplace(Value, '.', FormatSettings.DecimalSeparator, [rfReplaceAll]);
     Value := StringReplace(Value, ',', FormatSettings.DecimalSeparator, [rfReplaceAll]);
     Try
        Result := Value.ToExtended;
     Except
        Result := 0;
     End;
end;

end.
