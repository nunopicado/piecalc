unit uPIECalc;

interface

type
    IPIECalcView = Interface
      function Result(NewBlockingDate: TDateTime; RemainingDebt: Currency): IPIECalcView;
    End;

    IPIECalc = Interface
      function Eval: IPIECalc;
    End;

    TPIECalc = Class(TInterfacedObject, IPIECalc)
    private
      FPIECalcView         : IPIECalcView;
      FExpirationDate      : TDateTime;
      FCurrentBlockingDate : TDateTime;
      FTotalDebt           : Currency;
      FPaymentValue        : Currency;
      constructor Create(PIECalcView: IPIECalcView; ExpirationDate, CurrentBlockingDate: TDateTime; TotalDebt, PaymentValue: Currency);
      function DaysRemaining: Integer;
      function ValuePerDay: Currency;
    public
      class function New(PIECalcView: IPIECalcView; ExpirationDate, CurrentBlockingDate: TDateTime; TotalDebt, PaymentValue: Currency): IPIECalc;
      function Eval: IPIECalc;
    End;

implementation

uses
    SysUtils
  , DateUtils
  ;

{ TPIECalc }

constructor TPIECalc.Create(PIECalcView: IPIECalcView;
  ExpirationDate, CurrentBlockingDate: TDateTime; TotalDebt, PaymentValue: Currency);
begin
     FPIECalcView         := PIECalcView;
     FExpirationDate      := ExpirationDate;
     FCurrentBlockingDate := CurrentBlockingDate;
     FTotalDebt           := TotalDebt;
     FPaymentValue        := PaymentValue;
end;

function TPIECalc.DaysRemaining: Integer;
begin
     Result := DaysBetween(FCurrentBlockingDate, FExpirationDate);
end;

function TPIECalc.Eval: IPIECalc;
begin
     Result := Self;
     FPIECalcView.Result(IncDay(FCurrentBlockingDate, Round(FPaymentValue / ValuePerDay)), FTotalDebt-FPaymentValue);
end;

class function TPIECalc.New(PIECalcView: IPIECalcView;
  ExpirationDate, CurrentBlockingDate: TDateTime; TotalDebt, PaymentValue: Currency): IPIECalc;
begin
     Result := Create(PIECalcView, ExpirationDate, CurrentBlockingDate, TotalDebt, PaymentValue);
end;

function TPIECalc.ValuePerDay: Currency;
begin
     Result := FTotalDebt / DaysRemaining;
end;

end.
